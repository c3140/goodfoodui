import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import * as Environnement from 'src/app/Environnement'

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(request.url!=Environnement.CreateUserURL && request.url!=Environnement.LoginURL && (request.url!=Environnement.Menu || request.method!="GET") && (request.url!=Environnement.Restaurant || request.method!="GET"))
    {
      let TransformRequest = request.clone({
        setHeaders: {
          'Authorization': `Bearer `+localStorage.getItem("token")
        }
      })
      return next.handle(TransformRequest);
    }
    else
    {
      return next.handle(request);
    }
  }
}
