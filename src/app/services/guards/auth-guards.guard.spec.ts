import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuardsGuard } from './auth-guards.guard';

describe('AuthGuardsGuard', () => {
  let guard: AuthGuardsGuard;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthGuardsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
