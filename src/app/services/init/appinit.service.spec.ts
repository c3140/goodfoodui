import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AppinitService } from './appinit.service';

describe('AppinitService', () => {
  let service: AppinitService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppinitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
