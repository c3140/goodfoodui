import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as Environnement from 'src/app/Environnement'

@Injectable({
  providedIn: 'root'
})
export class AppinitService {
  constructor(private router: Router ) 
  { 
  }

  Init() {
    return new Promise<void>((resolve, reject) => {
      let diffdate = (new Date(Math.abs(<any>new Date()) - Math.abs(<any>new Date(localStorage.getItem('tokenValidTime')))).getTime()/1000) /60;
      if(localStorage.getItem("token")!=null && diffdate < Environnement.TimeoutTokenInMinutes)
      {
        this.router.navigateByUrl('/board');
      }
      if(localStorage.getItem("token")==null && this.router.url=='/board')
      {
        this.router.navigateByUrl('/');
      }

      if(localStorage.getItem("token")!=null && diffdate > Environnement.TimeoutTokenInMinutes)
      {
        localStorage.removeItem('token');
        localStorage.removeItem('tokenValidTime');
        localStorage.removeItem('username');
        this.router.navigateByUrl('/');
      }
      resolve();
    });
  }
}

export function initializeApp(appInitService: AppinitService) {
  return (): Promise<any> => { 
    return appInitService.Init();
  }
}

