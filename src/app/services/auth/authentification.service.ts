import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'src/app/model/auth/auth';
import { AuthResponse } from 'src/app/model/authResponse/auth-response';
import * as Environnement from 'src/app/Environnement'
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  datePipe: DatePipe
  constructor(private readonly http : HttpClient, private readonly router: Router) { }

  Login(auth : Auth) { 
    
    this.http.post<AuthResponse>(Environnement.LoginURL, auth, {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe( 
      (Response) => {
        localStorage.setItem('token', String(Response.token));
        localStorage.setItem('tokenValidTime', String(new Date()));
        localStorage.setItem('username', auth.username);
      },
      error =>{
        alert("Identifiant incorrect")
      },
      () =>
      {
        this.router.navigate(['/board']);
      }
    );
  }

}
