import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { firstValueFrom, lastValueFrom } from 'rxjs';
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-gestion-utilisateur',
  templateUrl: './gestion-utilisateur.component.html',
  styleUrls: ['./gestion-utilisateur.component.scss']
})
export class GestionUtilisateurComponent implements OnInit {

  @Input() user : User;

  addRoleUserForm: FormGroup;

  userSelect: User;

  constructor(readonly _httpClient:HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initFormSubscribe()
  }

  initFormSubscribe() {
    this.addRoleUserForm = this.formBuilder.group({
      userid: ['', [Validators.required]],
      role: ['', [Validators.required]]
    }
    );
  }

   public async addRole(){
    if(this.addRoleUserForm.valid)
    {
      const formValue = this.addRoleUserForm.value;
      let username = formValue['userid']
      await this.donnerUser(username)
      await this.userSelect.roles.push(formValue['role']);
      await this._httpClient.put<User>('https://localhost:8000/api/users/'+username, this.userSelect).
        subscribe(data => {
          this.userSelect = data
        });
    }
    else{
      alert("les champs ne sont pas correctement déclarés")
    }
  }

  public async donnerUser(username : string)
  {
    this.userSelect = new User()
    this.userSelect = await firstValueFrom(this._httpClient.get<User>("https://localhost:8000/api/users/"+username));
  }

}
