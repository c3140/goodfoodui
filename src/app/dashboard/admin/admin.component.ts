import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user/user';
import { DashboardComponent } from '../dashboard.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  @Input() user : User;

  showContainerPrivateComputer
  showContainerCellPhone

  visionFineActive=true;
  utilisateurActive=false;
  menusActive=false;
  restaurantActive=false;

  constructor(public breakpointObserver : BreakpointObserver, private router: Router) { }

  ngOnInit(): void {
    this.ajustScreen()
  }

  deconnexion(){
    localStorage.removeItem('token');
    localStorage.removeItem('tokenValidTime');
    localStorage.removeItem('username');
    this.router.navigateByUrl('/');
  }

  private ajustScreen() {
    this.breakpointObserver
      .observe(['(min-width: 768px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainerPrivateComputer = true;
          this.showContainerCellPhone = false;
        }

        else {
          this.showContainerPrivateComputer = false;
          this.showContainerCellPhone = true;
        }
      });
  }

  desactiveFonction(){
    this.visionFineActive=false;
    this.utilisateurActive=false;
    this.menusActive=false;
    this.restaurantActive=false;
  }

  activeVueVisionFine(){
    this.desactiveFonction();
    this.visionFineActive=true;
  }

  activeVueRestaurant(){
    this.desactiveFonction();
    this.restaurantActive=true;
  }

  activeVueMenus(){
    this.desactiveFonction();
    this.menusActive=true;
  }

  activeVueUtilisateur(){
    this.desactiveFonction();
    this.utilisateurActive=true;
  }

  activePageWithMenus(choix : string){
    if(choix === 'vision-fine')
    {
      this.activeVueVisionFine()
    }
    if(choix === 'restaurant')
    {
      this.activeVueRestaurant()
    }
    if(choix === 'menus')
    {
      this.activeVueMenus()
    }
  }

  redirectMyCompte(){
    this.router.navigateByUrl('/myaccount');
  }

}
