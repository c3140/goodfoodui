import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {faBars} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-menus-admin',
  templateUrl: './menus-admin.component.html',
  styleUrls: ['./menus-admin.component.scss']
})
export class MenusAdminComponent implements OnInit {

  @Output() choix = new EventEmitter<string>();

  faBars=faBars;

  menuaffiche : boolean

  constructor() { }

  ngOnInit(): void {
    this.menuaffiche = false
  }

  selectMenu(choix : string){
    this.choix.emit(choix)
  }

  afficherMenu(){
    if( this.menuaffiche == true) {
      this.menuaffiche = false
    }
    else
    {
      this.menuaffiche = true
    }
  }

}
