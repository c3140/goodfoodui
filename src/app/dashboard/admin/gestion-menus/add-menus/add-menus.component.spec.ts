import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { AddMenusComponent } from './add-menus.component';

describe('AddMenusComponent', () => {
  let component: AddMenusComponent;
  let fixture: ComponentFixture<AddMenusComponent>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, ReactiveFormsModule],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMenusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
