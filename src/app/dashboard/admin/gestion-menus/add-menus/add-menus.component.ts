import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Menus } from 'src/app/model/menu/menus';
import * as Environnement from 'src/app/Environnement'
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-add-menus',
  templateUrl: './add-menus.component.html',
  styleUrls: ['./add-menus.component.scss']
})
export class AddMenusComponent implements OnInit {

  MenusAddForm : FormGroup
  menu :Menus
  messageInformation : string
  activePop = false

  constructor(private readonly http :HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initFormMenusAdd();
  }

  initFormMenusAdd() {
    this.MenusAddForm = this.formBuilder.group({
      nameMenu: ['', Validators.required],
      priceMenu: ['', Validators.required],
      typeMenu: ['', Validators.required],
    }
    );

  }

  addMenus() {
    if(this.MenusAddForm.valid)
    {
      const formValue = this.MenusAddForm.value;
      this.menu = new Menus()
      this.menu.priceMenu = Number(formValue['priceMenu']);
      this.menu.nameMenu = formValue['nameMenu'];
      this.menu.typeMenu = formValue['typeMenu'];
      this.menu.idCountryMenu = "1";
      this.menu.createAt = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.menu.updateAt  = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.menu.numberRankMenu = 50
      this.http.post<Menus>(Environnement.Menu, this.menu).subscribe(res =>{
        this.messageInformation = "Menus "+this.menu.nameMenu+" a été ajouté"
        this.activePop = true
      })
      console.log(this.menu)
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
      this.activePop = true
    }
  }

  popoUpClose(close : boolean) {
    if(close == true)
    {
      this.activePop=false;
    }

  }

}
