import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Menus } from 'src/app/model/menu/menus';
import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-update-menus',
  templateUrl: './update-menus.component.html',
  styleUrls: ['./update-menus.component.scss']
})
export class UpdateMenusComponent implements OnInit {

  @Input() menu : Menus;

  @Output() annulation = new EventEmitter<boolean>();

  title: string
  MenusUpdateForm : FormGroup
  messageInformation : string
  activePop = false

  constructor(private readonly http :HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.title = "Menus à modifier"
    this.initFormMenusUpdate()
  }

  annulationUpdate() {
    this.annulation.emit(true)
  }

  initFormMenusUpdate() {
    this.MenusUpdateForm = this.formBuilder.group({
      prix: ['', Validators.required],
      nom: ['', Validators.required],
    }
    );

  }

  async updateMenu(){
    if(this.MenusUpdateForm.valid)
    {
      const formValue = this.MenusUpdateForm.value;

      this.menu.priceMenu = Number(formValue['prix']);
      this.menu.nameMenu = formValue['nom'];
      this.http.put<Menus>(Environnement.Menu+"/"+this.menu.id, this.menu).subscribe(res =>{
        this.messageInformation = "Menus "+this.menu.nameMenu+" a été mise à jour"
        this.activePop = true
      })
      console.log(this.menu)
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
      this.activePop = true
    }
  }

  popoUpClose(close : boolean) {
    if(close == true)
    {
      this.activePop=false;
    }

  }
}
