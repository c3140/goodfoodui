import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Menus } from 'src/app/model/menu/menus';

import * as Environnement from 'src/app/Environnement'
@Component({
  selector: 'app-gestion-menus',
  templateUrl: './gestion-menus.component.html',
  styleUrls: ['./gestion-menus.component.scss']
})
export class GestionMenusComponent implements OnInit {

  constructor(private readonly http : HttpClient) { }

  menusRecup : any[]
  menus : Menus[]
  menu : Menus;
  updateMenusAffichage = false
  numeroMenusModification : number

  async ngOnInit(): Promise<void> {
    await this.getListMenus()
    this.menus = this.menusRecup['hydra:member']
    console.log(this.menus)

  }

  async getListMenus(){
    this.menusRecup= await firstValueFrom(this.http.get<any[]>(Environnement.Menu));
  }

  deleteMenu(numero : number){

     this.http.delete(Environnement.Menu+"/"+numero).subscribe(()=>{
      alert("le menus a bien été supprimé")
    })
  }

  afficheUpdateMenus(numero : number){
    this.updateMenusAffichage = true;
    this.menus.forEach(element => {
      if(element.id === numero)
      {
        this.menu = element
      }
    })
  }

  annulationUpdate(annulation : boolean){
    if(annulation == true)
    {
      this.updateMenusAffichage=false;
    }
  }

}
