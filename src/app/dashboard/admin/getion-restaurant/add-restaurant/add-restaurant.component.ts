import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Restaurant } from 'src/app/model/restaurants/restaurant';
import * as Environnement from 'src/app/Environnement'
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.scss']
})
export class AddRestaurantComponent implements OnInit {

  RestaurantAddForm : FormGroup
  restaurant : Restaurant
  messageInformation : string
  activePop = false

  constructor(private readonly http :HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initFormRestaurantAdd();
  }

  initFormRestaurantAdd() {
    this.RestaurantAddForm = this.formBuilder.group({
      numSiretRest: [null, [Validators.required,Validators.minLength(14), Validators.maxLength(14)]],
      city: ['', Validators.required],
      adresseRest: ['', Validators.required],
      numPlaceRest: ['', Validators.required],
      numberStaffRest: ['', Validators.required],
      countryRest: [null, [Validators.required, Validators.maxLength(3)]],
    }
    );

  }

  addRestaurant() {
    if(this.RestaurantAddForm.valid)
    {
      const formValue = this.RestaurantAddForm.value;
      this.restaurant = new Restaurant()
      this.restaurant.numSiretRest = formValue['numSiretRest'];
      this.restaurant.city = formValue['city'];
      this.restaurant.adresseRest = formValue['adresseRest'];
      this.restaurant.numPlaceRest = Number(formValue['numPlaceRest']);
      this.restaurant.numberStaffRest = Number(formValue['numberStaffRest']);
      this.restaurant.countryRest = formValue['countryRest'];
      this.restaurant.ranknumber=0
      this.restaurant.timeOpenRest=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.restaurant.timeCloseRest=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.restaurant.createAt = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.restaurant.updateAt  = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.restaurant.numberVisitRest=0
      this.restaurant.caRest=0.0
      this.restaurant.satisfactionRateRest=100.0
      this.http.post<Restaurant>(Environnement.Restaurant, this.restaurant).subscribe(res =>{
        this.messageInformation = "Le restaurant "+this.restaurant.numSiretRest+" à "+this.restaurant.city +" a été ajouté"
        this.activePop = true
      },
      error =>{
        this.messageInformation = "La requête demandée n'a pas pu être exécuté"
        this.activePop = true
      }
      )
      console.log(this.restaurant)
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
      this.activePop = true
    }
  }

  popoUpClose(close : boolean) {
    if(close == true)
    {
      this.activePop=false;
    }

  }

}
