import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetionRestaurantComponent } from './getion-restaurant.component';

describe('GetionRestaurantComponent', () => {
  let component: GetionRestaurantComponent;
  let fixture: ComponentFixture<GetionRestaurantComponent>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetionRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetionRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
