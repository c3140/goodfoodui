import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Restaurant } from 'src/app/model/restaurants/restaurant';
import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-getion-restaurant',
  templateUrl: './getion-restaurant.component.html',
  styleUrls: ['./getion-restaurant.component.scss']
})
export class GetionRestaurantComponent implements OnInit {

  constructor(private readonly http : HttpClient) { }

  restRecup : any[]
  restaurants : Restaurant[]
  restaurant : Restaurant;
  updateMenusAffichage = false
  numeroMenusModification : number

  async ngOnInit(): Promise<void> {
    await this.getListRestaurant()
    this.restaurants = this.restRecup['hydra:member']
    console.log(this.restaurant)

  }

  async getListRestaurant(){
    this.restRecup= await firstValueFrom(this.http.get<any[]>(Environnement.Restaurant));
  }

  deleteMenu(numero : number){

     this.http.delete(Environnement.Restaurant+"/"+numero).subscribe(()=>{
      alert("le restaurant à bien été supprimé")
    })
  }

  afficheUpdateMenus(numero : number){
    this.updateMenusAffichage = true;
    this.restaurants.forEach(element => {
      if(element.id === numero)
      {
        this.restaurant = element
      }
    })
  }

  annulationUpdate(annulation : boolean){
    if(annulation == true)
    {
      this.updateMenusAffichage=false;
    }
  }
}
