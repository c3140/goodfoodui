import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as Environnement from 'src/app/Environnement'
import { Restaurant } from 'src/app/model/restaurants/restaurant';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.scss']
})
export class UpdateRestaurantComponent implements OnInit {

  @Input() restaurant : Restaurant;

  @Output() annulation = new EventEmitter<boolean>();

  title: string
  RestaurantUpdateForm : FormGroup
  messageInformation : string
  activePop = false

  constructor(private readonly http :HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.title = "Restaurant à modifier dont le SIRET est : "+this.restaurant.numSiretRest
    this.initFormRestaurantUpdate()
  }

  annulationUpdate() {
    this.annulation.emit(true)
  }

  initFormRestaurantUpdate() {
    this.RestaurantUpdateForm = this.formBuilder.group({
      numplace: ['', Validators.required],
      numStaff: ['', Validators.required],
    }
    );

  }

  async updateMenu(){
    if(this.RestaurantUpdateForm.valid)
    {
      const formValue = this.RestaurantUpdateForm.value;

      this.restaurant.numPlaceRest = Number(formValue['numplace']);
      this.restaurant.numberStaffRest = Number(formValue['numStaff']);
      this.http.put<Restaurant>(Environnement.Restaurant+"/"+this.restaurant.id, this.restaurant).subscribe(res =>{
        this.messageInformation = "Restaurant "+this.restaurant.numSiretRest+" a été mise à jour"
        this.activePop = true
      })
      console.log(this.restaurant)
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
      this.activePop = true
    }
  }

  popoUpClose(close : boolean) {
    if(close == true)
    {
      this.activePop=false;
    }

  }

}
