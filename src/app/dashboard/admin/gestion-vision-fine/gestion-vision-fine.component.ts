import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Commande } from 'src/app/model/commande/commande';
import { ListCommandes } from 'src/app/model/listcommande/list-commandes';
import { User } from 'src/app/model/user/user';

import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-gestion-vision-fine',
  templateUrl: './gestion-vision-fine.component.html',
  styleUrls: ['./gestion-vision-fine.component.scss']
})
export class GestionVisionFineComponent implements OnInit {

  constructor(private readonly http : HttpClient) { }

  commande : Commande[];
  listCommandeRecup : ListCommandes[]
  listCommande : any[];
  users : User[]

  prixTotal = 0;

  async ngOnInit(): Promise<void> {
    await this.getCommande()
  }

  getCommande()
  {
    this.http.get<Commande[]>(Environnement.Commande).subscribe(
      async res=>{
        this.commande = await Array.from(Object.values(res['hydra:member']));
      }
    );
  }

  async getUser(){
    await Promise.all(this.listCommandeRecup.map(async (element) =>{
      this.users.push(await firstValueFrom(this.http.get<User>(element.user)));
    }))
  }

  calculTotal(){
    this.commande.forEach((currentValue, index) => {
      this.prixTotal = this.prixTotal + currentValue.price_global;
      console.log(this.prixTotal)
    });
  }



}
