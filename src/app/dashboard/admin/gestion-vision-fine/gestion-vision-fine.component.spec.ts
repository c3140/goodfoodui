import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionVisionFineComponent } from './gestion-vision-fine.component';

describe('GestionVisionFineComponent', () => {
  let component: GestionVisionFineComponent;
  let fixture: ComponentFixture<GestionVisionFineComponent>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionVisionFineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionVisionFineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
