import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Menus } from 'src/app/model/menu/menus';
import * as Environnement from 'src/app/Environnement'
import { HttpClient } from '@angular/common/http';
import { ListCommandes } from 'src/app/model/listcommande/list-commandes';
import { Commande } from 'src/app/model/commande/commande';
import { firstValueFrom } from 'rxjs';
import { User } from 'src/app/model/user/user';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-panier-commande',
  templateUrl: './panier-commande.component.html',
  styleUrls: ['./panier-commande.component.scss']
})
export class PanierCommandeComponent implements OnInit {

  @Input() menusSelectionne : Menus[]
  @Input() nombreMenu : number
  @Input() user : User;

  messageInformation : string

  constructor(private readonly http : HttpClient) { }

  menusListePanier : Menus[]

  commandeeffectue =false;

  prixTotal = 0

  commande : any

  ngOnInit(): void {
    this.menusListePanier = this.menusSelectionne
  }

  calculerLePrix(){
    this.menusListePanier.forEach(element =>{
      this.prixTotal = this.prixTotal + element.priceMenu
    })
  }

  async commander() {
    let commande = new Commande()
    commande.validCommande=true
    commande.satisfactionCommande=0.0
    commande.priceGlobal=this.prixTotal
    commande.createAt = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
    commande.updateAt  = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
    commande.username = this.user.username
    this.commande = await firstValueFrom(this.http.post(Environnement.Commande, commande))
    if(this.commande !== null)
    {
      this.messageInformation = "Votre commande a bien été effectué"
      this.commandeeffectue = true
    }
    else{
      this.messageInformation = "Un problème est survenu, veuillez ressayer plus tard"
    }
  }

}
