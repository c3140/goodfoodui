import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Menu } from 'src/app/Environnement';
import { Menus } from 'src/app/model/menu/menus';

import { PanierCommandeComponent } from './panier-commande.component';

describe('PanierCommandeComponent', () => {
  let component: PanierCommandeComponent;
  let fixture: ComponentFixture<PanierCommandeComponent>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanierCommandeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanierCommandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('le transfert entre les tableau doit être effectue', () => {
    component.ngOnInit();
    expect(component.menusListePanier).not.toBeNull()
  });
});
