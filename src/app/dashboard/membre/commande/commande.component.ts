import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Menus } from 'src/app/model/menu/menus';
import * as Environnement from 'src/app/Environnement'
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss']
})
export class CommandeComponent implements OnInit {

  @Input() user : User;

  menusRecup : any[]
  menus : Menus[]

  listMenusSelect : Menus[]

  addClick = 0;

  numberMenusAdd=0

  pannierAffiche = false

  constructor(private readonly http : HttpClient) { }

  async ngOnInit(): Promise<void> {
    await this.getListMenus()
    this.menus = this.menusRecup['hydra:member']
    this.listMenusSelect = new Array<Menus>()
    console.log(this.menus)
  }

  async getListMenus(){
    this.menusRecup= await firstValueFrom(this.http.get<any[]>(Environnement.Menu));
  }

  addToListe(numMenus : number, prixMenus : Number, nomMenus : string ) {
    let men = new Menus()
    men.id=numMenus
    men.nameMenu=nomMenus
    men.priceMenu=prixMenus
    this.listMenusSelect.push(men)
    this.numberMenusAdd = this.numberMenusAdd + 1
    console.log(this.listMenusSelect)
  }

  removeToListe(numMenus : number, prixMenus : Number, nomMenus : string){
    var menuss = this.listMenusSelect.find(s => s.id === numMenus)
    var index = this.listMenusSelect.indexOf(menuss)

    if(index > -1){
      this.listMenusSelect.splice(index,1)
      this.numberMenusAdd = this.numberMenusAdd - 1
    }
    console.log(this.listMenusSelect)
  }

  afficherPanier(){
    if(this.pannierAffiche===false){
      this.pannierAffiche = true
    }
    else{
      this.pannierAffiche = false
    }
  }

}
