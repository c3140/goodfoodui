import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-membre',
  templateUrl: './membre.component.html',
  styleUrls: ['./membre.component.scss']
})
export class MembreComponent implements OnInit {

  @Input() user : User;

  constructor(private router: Router) { }

  commandeActive=true;
  reservationActive=false;
  informationActive=false

  async ngOnInit(): Promise<void> {

  }

  deconnexion(){
    localStorage.removeItem('token');
    localStorage.removeItem('tokenValidTime');
    localStorage.removeItem('username');
    this.router.navigateByUrl('/');
  }

  desactiveFonction(){
    this.commandeActive=false;
    this.reservationActive=false;
    this.informationActive=false;
  }

  activeVueCommande(){
    this.desactiveFonction();
    this.commandeActive=true;
  }

  activeVueReservation(){
    this.desactiveFonction();
    this.reservationActive=true;
  }

  activeVueInformation(){
    this.desactiveFonction();
    this.informationActive=true;
  }

  redirectMyCompte(){
    this.router.navigateByUrl('/myaccount');
  }

}
