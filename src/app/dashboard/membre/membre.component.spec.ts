import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MembreComponent } from './membre.component';

describe('MembreComponent', () => {
  let component: MembreComponent;
  let fixture: ComponentFixture<MembreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
    })
    .compileComponents();
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MembreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MembreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const localStorageMock = (() => {
    let store = {};
    return {
      getItem(key) {
        return store[key];
      },
      setItem(key, value) {
        store[key] = value.toString();
      },
      clear() {
        store = {};
      },
      removeItem(key) {
        delete store[key];
      }
    };
  })();
  Object.defineProperty(window, 'localStorage', {
    value: localStorageMock
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('deconnexion fn', function(){

    it('should delete localstorage', function() {
      const KEY = 'username',
        VALUE = 'bar';

        localStorageMock.setItem(KEY, VALUE)
        component.deconnexion()
        expect(localStorageMock.getItem(KEY)).toBeNull

    });

  });

  describe('desactiveFonction fn', function(){

    it('should all active in false', function() {
      component.commandeActive=true;
      component.informationActive=true;
      component.reservationActive=true;

      component.desactiveFonction()

      expect(component.commandeActive).toBeFalsy
      expect(component.informationActive).toBeFalsy
      expect(component.reservationActive).toBeFalsy

    });

  });
});
