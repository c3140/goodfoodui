import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Commande } from 'src/app/model/commande/commande';

import * as Environnement from 'src/app/Environnement'
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

  @Input() user : User

  commande : Commande[];
  commandeUser : Commande[];

  afficheCommande = false

  constructor(private readonly http : HttpClient) { }

  async ngOnInit():Promise<void> {
    await this.getCommande()
  }

  getCommande()
  {
    this.http.get<Commande[]>(Environnement.Commande).subscribe(
      async res=>{
        this.commande = await Array.from(Object.values(res['hydra:member']));
      }
    );
  }

  public getCommandeForUser(){
    this.commandeUser = new Array<Commande>();
     this.commande.forEach(element => {
      if(element.username === this.user.username) {
        this.commandeUser.push(element)
      }
    })
    this.afficherTableauCommande()
  }

  public afficherTableauCommande() {
    if(this.getCommandeForUser !== null){
      this.afficheCommande = true
    }
  }

}
