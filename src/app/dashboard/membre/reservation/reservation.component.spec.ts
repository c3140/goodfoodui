import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Restaurant } from 'src/app/model/restaurants/restaurant';
import { of } from 'rxjs';

import { ReservationComponent } from './reservation.component';

describe('ReservationComponent', () => {
  let component: ReservationComponent;
  let fixture: ComponentFixture<ReservationComponent>;
  let http : HttpClient

  beforeEach(() => TestBed.configureTestingModule({
    imports: [],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationComponent ],
      imports : [HttpClientTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    http = TestBed.inject(HttpClient)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getListRestaurant', function(){

    it('should return a list of restaurant', function() {
      jest.spyOn(http, 'get').mockReturnValue(of({id : 1}))
      component.getListRestaurant()
      expect(component.restRecup).not.toBeNull
    });

  });


});
