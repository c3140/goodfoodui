import { formatDate, NumberSymbol } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Reservation } from 'src/app/model/reservation/reservation';
import { Restaurant } from 'src/app/model/restaurants/restaurant';

import * as Environnement from 'src/app/Environnement'
import { MatCalendar } from '@angular/material/datepicker';
import { Data } from '@angular/router';

@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.component.html',
  styleUrls: ['./add-reservation.component.scss']
})
export class AddReservationComponent implements OnInit {

  yearActual : Number
  monthActual : Number
  dayActual : Number

  maxYearDate : Number
  maxMonthActual : Number
  maxDayActual : Number

  @ViewChild('calendar') calendar: MatCalendar<Date>;
 selectedDate: Date;

  @Input() restaurant : Restaurant[]

  ReservationAddForm : FormGroup
  messageInformation : string
  activePop = false

  reservation : Reservation

  constructor(private readonly http :HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.yearActual = new Date().getFullYear();
    this.monthActual = new Date().getMonth();
    this.dayActual = new Date().getDate();
    this.maxYearDate = new Date().getFullYear() + 3;
    this.maxMonthActual = new Date().getMonth() + 3;
    this.maxDayActual = new Date().getDate() + 3;

    console.log(this.restaurant)
    this.initFormReservationAdd()
  }


  initFormReservationAdd() {
    this.ReservationAddForm = this.formBuilder.group({
      adresseRestaurant: [null, Validators.required],
      numberCustomers: ['', Validators.required],
      heure: ['', [Validators.required, Validators.min(11), Validators.max(23)]],
      minute: ['', [Validators.required, Validators.min(0), Validators.max(59)]],
      date: ['', Validators.required],
    }
    );

  }

  addReservation() {
    const formValue = this.ReservationAddForm.value;
    if(this.ReservationAddForm.valid && this.checkdate(formValue['date']) && this.checkHours(Number(formValue['heure']), Number(formValue['minute'])))
    {
      this.reservation = new Reservation()
      let datereser=formValue['date']+" "+formValue['heure']+":"+formValue['minute'];
      this.reservation.dateReservationAt=datereser;
      this.reservation.adresseRestaurant=formValue['adresseRestaurant']
      this.reservation.numberCustomers=Number(formValue['numberCustomers'])
      this.reservation.createAt = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.reservation.updateAt  = formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
      this.reservation.username = localStorage.getItem("username")
      this.http.post<Reservation>(Environnement.Reservation, this.reservation).subscribe(res =>{
        this.messageInformation = "La réservation a été ajouté"
      },
      error =>{
        this.messageInformation = "La demande n'a pas pu être effectuée"
      }
      )
      console.log(this.restaurant)
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
    }
  }

  checkdate(date : string) : boolean{

    var date2 = new Date(date);

    if(date2.getFullYear() >= this.yearActual){
      if(date2.getMonth() >= this.monthActual)
      {
        if(date2.getDate() < this.dayActual && date2.getMonth() > this.monthActual){
          return true
        }
        if(date2.getDate() > this.dayActual){
          return true
        }
        if(date2.getDate() == this.dayActual) {
          return true
        }
        else{
          return false
        }
      }
      else{
        return false
      }
    }
    else{
      return false
    }
  }

  checkHours(hours : Number, minutes : Number) : boolean{
    let date = new Date()
    if(date.getHours() < hours){
      return true;
    }
    else{
      if(date.getHours() === hours) {
        if(date.getMinutes() < minutes){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    }
  }

}
