import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { Restaurant } from 'src/app/model/restaurants/restaurant';

import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  restRecup : any[]
  restaurants : Restaurant[]

  reservationAffiche =false

  constructor(private readonly http :HttpClient) { }

  async ngOnInit(): Promise<void> {
    await this.getListRestaurant()
    this.restaurants = await this.restRecup['hydra:member']
  }

  async getListRestaurant(){
    this.restRecup= await firstValueFrom(this.http.get<any[]>(Environnement.Restaurant));
  }

  afficherReservation() {
    this.reservationAffiche = true
  }



}
