import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { first, firstValueFrom } from 'rxjs';
import { User } from '../model/user/user';

import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  activeMembre: boolean;
  activeAdmin: boolean;

  constructor(private http : HttpClient) { }

  user : User;

  async ngOnInit(): Promise<void> {
    await this.donnerUser();
    this.getRoleUser();
  }

  public async donnerUser()
  {
    this.user = new User()
    let username = localStorage.getItem('username');
    this.user = await firstValueFrom(this.http.get<User>(Environnement.User+'/'+username));
  }

  public getRoleUser(){
    if(this.user.roles.indexOf("ROLE_ADMIN")!==-1){
      this.activeMembre=false;
      this.activeAdmin=true;
    }
    else {
      this.activeMembre=true;
      this.activeAdmin=false;
    }
  }

}
