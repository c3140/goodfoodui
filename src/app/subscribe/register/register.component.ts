import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user/user';

import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  //Le titre est amené à changer en fonction des différentes partie
  Title : String;

  //Cette Varaible permet de réaliser un formulaire
  SubscribeForm: FormGroup;

  //Variable pour la date de naissance
  Yearnow;
  Daynow;
  Monthnow;
  DayArray;
  YearArray;

  yearnumber: number;

  user : User

  constructor(readonly _httpClient:HttpClient, private formBuilder: FormBuilder, private _router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') !== null){
      this._router.navigateByUrl('/member');
    }
    this.Title="CRÉER UN COMPTE";
    this.initFormSubscribe();
    //on crée une instance de la classe date puis on récupère l'année actuelle
    //qui permettra d'afficher les années dans le tag spécifié
    var datenow = new Date();
    this.Yearnow=datenow.getFullYear();
    this.Monthnow=datenow.getMonth();
    this.Daynow=datenow.getDate();

    //on crée des tableau pour les tags de la date de naissance
    this.DayArray = new Array(31);
    this.YearArray=new Array(101);
  }

  initFormSubscribe() {
    this.SubscribeForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dayBirth: [null, Validators.required],
      monthBirth: [null, Validators.required],
      yearBirth: [null, Validators.required],
      city: [null, Validators.required],
      address: [null, Validators.required],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      username: ['', Validators.required],
    }
    );

  }


  CreatUser()
  {
    const formValue = this.SubscribeForm.value;

    if(this.valideBirthday(Number(formValue['yearBirth']), Number(formValue['dayBirth']), Number(formValue['monthBirth'])))
    {
        this.user = new User()
        this.user.birthdateUser=formValue['yearBirth']+'-'+formValue['monthBirth']+'-'+formValue['dayBirth']
        this.user.firstnameUser=formValue['firstName']
        this.user.lastnameUser=formValue['lastName']
        this.user.cityUser=formValue['city']
        this.user.addressUser=formValue['address']
        this.user.email=formValue['email']
        this.user.password=formValue['password']
        this.user.username=formValue['username']
        this.user.bonusReducUser=0
        this.user.numberVisitUser=0
        this.user.createAt=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
        this.user.updateAt=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
        this.user.last_commande_user=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")
        this.user.last_visit_user=formatDate(new Date(), 'yyyy-MM-dd hh:mm:ss', "en")

        this._httpClient.post<User>(Environnement.User, this.user).subscribe(res =>{
          alert("l'utilisateur a bien été ajouté")
        })

    }
  }

  //Cette fonction valide la date de naissance
  valideBirthday(yearBirth, monthBirth, dayBirth): Boolean
  {
    if(monthBirth > 12 || monthBirth < 1 || dayBirth > 31 || dayBirth < 1)
    {
      return false;
    }
    if((monthBirth==4 || monthBirth==6 || monthBirth==9 || monthBirth==11) && dayBirth==31)
    {
      return false;
    }
    if(this.Yearnow-yearBirth>=18)
    {
      if(monthBirth==2 && (dayBirth==30 || dayBirth==31))
      {
        return false;
      }
      else
      {
        if(monthBirth==2 && dayBirth==29)
        {
            if(yearBirth%4==0)
            {
                if(yearBirth%100==0)
                {
                  return true;
                }
                else
                {
                  if(yearBirth%400==0)
                  {
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }

      }

    }
    else
    {
      return false;
    }

  }

}
