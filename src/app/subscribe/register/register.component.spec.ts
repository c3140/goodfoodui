import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, 
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [ RegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('La date renseigné doit être valide', () => {
    expect(component.valideBirthday(1999, 11, 20)).toBeTruthy();
    expect(component.valideBirthday(2000, 2, 29)).toBeTruthy();
    expect(component.valideBirthday(1993, 1, 31)).toBeTruthy();
    expect(component.valideBirthday(2004, 1, 4)).toBeTruthy();
  });
  
  it('La date renseigné ne doit pas être valide', () => {
    expect(component.valideBirthday(1999, 2, 29)).toBeFalsy();
    expect(component.valideBirthday(1999, 4, 31)).toBeFalsy();
    expect(component.valideBirthday(1993, 1, 43)).toBeFalsy();
    expect(component.valideBirthday(2005, 1, 31)).toBeFalsy();
  });
});
