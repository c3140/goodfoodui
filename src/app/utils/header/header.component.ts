import { Component, OnInit } from '@angular/core';
import { BreakpointObserver,  BreakpointState} from '@angular/cdk/layout';
import { Router } from '@angular/router';
import {faUser, faBars} from '@fortawesome/free-solid-svg-icons';
import {faMap} from '@fortawesome/free-regular-svg-icons';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public showContainerPrivateComputer : Boolean;
  public showContainerCellPhone : Boolean;
  public option_sign : Boolean;
  public option_return : Boolean;

  title_cellphone = true;

  faUser=faUser;
  faMap=faMap;
  faBars=faBars;

  styleHeader : String;

  constructor(public breakpointObserver : BreakpointObserver, private router: Router) { }

  ngOnInit(): void {
    this.ajustScreen();
    this.selectOptionInHeader();
  }

  private ajustScreen() {
    this.breakpointObserver
      .observe(['(min-width: 768px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainerPrivateComputer = true;
          this.showContainerCellPhone = false;
        }

        else {
          this.showContainerPrivateComputer = false;
          this.showContainerCellPhone = true;
        }
      });
  }

  private selectOptionInHeader() {
    if (this.router.url.startsWith("/")) {
      this.option_return = false;
      this.option_sign=true;
    }
    if(this.router.url.startsWith("/subscribe") || this.router.url.startsWith("/signin")){
      this.option_return = true;
      this.option_sign=false;
    }
    if(this.router.url.startsWith("/board") || this.router.url.startsWith("/myaccount")){
      this.option_sign =false;
      this.title_cellphone=false;
    }
  }

  public returnHome() {
    this.router.navigate(['/']);
  }

  public redirectSubscribeComposant() {
    this.router.navigate(['/subscribe']);
  }

  public redirectConnectionComposant() {
    this.router.navigate(['/signin']);
  }

  public redirectMenusComposant(){
    this.router.navigate(['/menus']);
  }

  public redirectRestaurantComposant(){
    this.router.navigate(['restaurant'])
  }
}
