import { Component, OnInit } from '@angular/core';
import { BreakpointObserver,  BreakpointState} from '@angular/cdk/layout';
import { faTwitter, faFacebookSquare, faLinkedin, faYoutube } from '@fortawesome/free-brands-svg-icons'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public showContainerPrivateComputer : Boolean;
  public annee_en_cour : Number;

  faTwitter = faTwitter;
  faFacebookSquare = faFacebookSquare;
  faLinkedin = faLinkedin;
  faYoutube=faYoutube;

  constructor(public breakpointObserver : BreakpointObserver) { }

  ngOnInit(): void {
    this.breakpointObserver
    .observe(['(min-width: 768px)'])
    .subscribe((state: BreakpointState) =>{
      state.matches ? this.showContainerPrivateComputer=true :  this.showContainerPrivateComputer=false;
    });

    this.annee_en_cour=new Date().getFullYear();
  }

}
