import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { User } from '../model/user/user';

import * as Environnement from 'src/app/Environnement'
import { Router } from '@angular/router';

@Component({
  selector: 'app-gestion-compte',
  templateUrl: './gestion-compte.component.html',
  styleUrls: ['./gestion-compte.component.scss']
})
export class GestionCompteComponent implements OnInit {

  constructor(private readonly http :HttpClient) { }

  user : User;
  userfound=false

  async ngOnInit(): Promise<void> {
    await this.donnerUser()
  }


  public async donnerUser()
  {
    this.user = new User()
    let username = localStorage.getItem('username');
    this.user = await firstValueFrom(this.http.get<User>(Environnement.User+"/"+username));
    this.userfound = this.foundUser()
  }

  public foundUser()
  {
    return true
  }

}
