import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import * as Environnement from 'src/app/Environnement'
import { User } from 'src/app/model/user/user';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  @Input() user: User

  constructor(private readonly http :HttpClient , private formBuilder : FormBuilder, private router: Router) { }

  userUpdateForm : FormGroup

  messageInformation : string

  activePop = false

  ngOnInit(): void {
    this.initFormUserUpdate()
  }

  updateUser(){
    if(this.userUpdateForm.valid)
    {
      const formValue = this.userUpdateForm.value;
      this.user.password = formValue['motdepasse'];
      this.user.addressUser = formValue['address'];

      this.http.put<User>(Environnement.User+"/"+this.user.username, this.user).subscribe(res =>{
        this.messageInformation = "L'utilisateur a été mise à jour"
        this.activePop = true
      })
    }
    else{
      this.messageInformation = "Les champs ne sont pas déclaré correctement"
      this.activePop = true
    }
  }

   initFormUserUpdate() {
    this.userUpdateForm =  this.formBuilder.group({
      motdepasse: ['', Validators.required],
      address : ['', Validators.required]
    }
    );
  }

  popoUpClose(close : boolean) {
    if(close == true)
    {
      this.activePop=false;
    }
  }

  public async deleteAccount(){
    localStorage.removeItem('token');
    localStorage.removeItem('tokenValidTime');
    localStorage.removeItem('username');
    await this.http.delete(Environnement.User+'/'+this.user.username).subscribe(()=>{
      alert("l'utilisateur a bien été supprimé")
    })
    this.router.navigateByUrl('/');
  }

}
