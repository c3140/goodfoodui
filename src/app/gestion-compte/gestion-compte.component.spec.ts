import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionCompteComponent } from './gestion-compte.component';

describe('GestionCompteComponent', () => {
  let component: GestionCompteComponent;
  let fixture: ComponentFixture<GestionCompteComponent>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionCompteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('utilisateur a été trouve', () => {
    expect(component.foundUser).toBeTruthy();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
