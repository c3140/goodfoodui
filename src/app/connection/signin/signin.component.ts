import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Auth } from 'src/app/model/auth/auth';
import { AuthentificationService } from 'src/app/services/auth/authentification.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  //Le titre est amené à changer en fonction des différentes partie
  Title : String;
  auth : Auth;
  authService : AuthentificationService;

  //Cette Varaible permet de réaliser un formulaire
  SubscribeForm: FormGroup;
  
  constructor(readonly _httpClient:HttpClient, private formBuilder: FormBuilder, private _router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('token') !== null){
      this._router.navigateByUrl('/board');
    }
    this.Title="CONNECTEZ-VOUS";
    this.initFormSubscribe();
    this.authService=new AuthentificationService(this._httpClient, this._router);
  }


  initFormSubscribe() {
    this.SubscribeForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    }
    );
  }

  public redirectSubscribeComposant() {
    this._router.navigate(['/subscribe']);
  }

  public connection(){
    if(this.SubscribeForm.valid)
    {
      const formValue = this.SubscribeForm.value;
      this.auth = new Auth();
      this.auth.username = formValue['username'];
      this.auth.password = formValue['password']
      this.authService.Login(this.auth);
    }
    else{
      alert("les champs ne sont pas correctement déclarés")
    }
  }

}
