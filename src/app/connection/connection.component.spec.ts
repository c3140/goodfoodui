import { HttpClient, HttpHandler } from '@angular/common/http';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FooterComponent } from '../utils/footer/footer.component';
import { HeaderComponent } from '../utils/header/header.component';

import { ConnectionComponent } from './connection.component';

describe('ConnectionComponent', () => {
  let component: ConnectionComponent;
  let fixture: ComponentFixture<ConnectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserModule, FormsModule, ReactiveFormsModule],
      declarations: [ ConnectionComponent, FooterComponent, HeaderComponent ],
      providers: [HttpClient, HttpHandler, FormBuilder, RouterTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    inject([RouterTestingModule, HttpClient, FormBuilder],(dep, object)=>{})
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
