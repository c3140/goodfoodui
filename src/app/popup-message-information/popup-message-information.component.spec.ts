import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupMessageInformationComponent } from './popup-message-information.component';

describe('PopupMessageInformationComponent', () => {
  let component: PopupMessageInformationComponent;
  let fixture: ComponentFixture<PopupMessageInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupMessageInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupMessageInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
