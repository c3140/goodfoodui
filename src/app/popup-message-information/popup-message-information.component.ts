import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-popup-message-information',
  templateUrl: './popup-message-information.component.html',
  styleUrls: ['./popup-message-information.component.scss']
})
export class PopupMessageInformationComponent implements OnInit {

  @Input() message : string
  @Input() active : boolean
  @Output() annulation = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  closePopup() {
    this.active = false
    this.annulation.emit(true)
  }

}
