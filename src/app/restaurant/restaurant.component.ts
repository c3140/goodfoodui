import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../model/restaurants/restaurant';

import * as Environnement from 'src/app/Environnement'
@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {

  constructor(private readonly http:HttpClient) { }

  restaurant: Restaurant[];

  ngOnInit(): void {
    this.getRestaurant();
  }

  getRestaurant()
  {
    this.http.get<Restaurant[]>(Environnement.Restaurant).subscribe(
      res=>{
        this.restaurant = Array.from(Object.values(res['hydra:member']));
      }
    );
  }

}
