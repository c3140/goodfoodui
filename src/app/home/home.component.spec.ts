import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { DescriptionComponent } from '../description/description.component';
import { FooterComponent } from '../utils/footer/footer.component';
import { HeaderComponent } from '../utils/header/header.component';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
     TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserModule, FormsModule, ReactiveFormsModule],
      declarations: [ HomeComponent, FooterComponent, HeaderComponent, DescriptionComponent],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
