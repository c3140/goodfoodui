import { Component, OnInit } from '@angular/core';
import { BreakpointObserver,  BreakpointState} from '@angular/cdk/layout';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
  public showContainerPrivateComputer : Boolean;
  constructor( public breakpointObserver : BreakpointObserver) { }
  ngOnInit(): void {
    this.breakpointObserver
    .observe(['(min-width: 768px)'])
    .subscribe((state: BreakpointState) =>{
      state.matches ? this.showContainerPrivateComputer=true :  this.showContainerPrivateComputer=false;
    });
  }

}
