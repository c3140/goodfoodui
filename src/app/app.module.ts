import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './utils/header/header.component';
import { FooterComponent } from './utils/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DescriptionComponent } from './description/description.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { RegisterComponent } from './subscribe/register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ConnectionComponent } from './connection/connection.component';
import { AppinitService, initializeApp } from './services/init/appinit.service';
import { MenusComponent } from './menus/menus.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { SigninComponent } from './connection/signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MembreComponent } from './dashboard/membre/membre.component';
import { AdminComponent } from './dashboard/admin/admin.component';
import { CommandeComponent } from './dashboard/membre/commande/commande.component';
import { InformationComponent } from './dashboard/membre/information/information.component';
import { GestionUtilisateurComponent } from './dashboard/admin/gestion-utilisateur/gestion-utilisateur.component';
import { GestionMenusComponent } from './dashboard/admin/gestion-menus/gestion-menus.component';
import { GetionRestaurantComponent } from './dashboard/admin/getion-restaurant/getion-restaurant.component';
import { GestionVisionFineComponent } from './dashboard/admin/gestion-vision-fine/gestion-vision-fine.component';
import { InterceptorInterceptor } from './services/interceptor/interceptor.interceptor';
import { MenusAdminComponent } from './dashboard/admin/menus-admin/menus-admin.component';
import { UpdateMenusComponent } from './dashboard/admin/gestion-menus/update-menus/update-menus.component';
import { PopupMessageInformationComponent } from './popup-message-information/popup-message-information.component';
import { AddMenusComponent } from './dashboard/admin/gestion-menus/add-menus/add-menus.component';
import { AddRestaurantComponent } from './dashboard/admin/getion-restaurant/add-restaurant/add-restaurant.component';
import { UpdateRestaurantComponent } from './dashboard/admin/getion-restaurant/update-restaurant/update-restaurant.component';
import { PanierCommandeComponent } from './dashboard/membre/commande/panier-commande/panier-commande.component';
import { ReservationComponent } from './dashboard/membre/reservation/reservation.component';
import { GestionCompteComponent } from './gestion-compte/gestion-compte.component';
import { UpdateUserComponent } from './gestion-compte/update-user/update-user.component';
import { AddReservationComponent } from './dashboard/membre/reservation/add-reservation/add-reservation.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    DescriptionComponent,
    SubscribeComponent,
    RegisterComponent,
    ConnectionComponent,
    MembreComponent,
    CommandeComponent,
    MenusComponent,
    RestaurantComponent,
    InformationComponent,
    SigninComponent,
    DashboardComponent,
    AdminComponent,
    GestionUtilisateurComponent,
    GestionMenusComponent,
    GetionRestaurantComponent,
    GestionVisionFineComponent,
    MenusAdminComponent,
    UpdateMenusComponent,
    PopupMessageInformationComponent,
    AddMenusComponent,
    AddRestaurantComponent,
    UpdateRestaurantComponent,
    PanierCommandeComponent,
    ReservationComponent,
    GestionCompteComponent,
    UpdateUserComponent,
    AddReservationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule
  ],
  providers: [
    AppinitService,
    { provide: APP_INITIALIZER,useFactory: initializeApp, deps: [AppinitService], multi: true},
    {
      provide : HTTP_INTERCEPTORS,
      useClass: InterceptorInterceptor,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
