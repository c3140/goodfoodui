import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectionComponent } from './connection/connection.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GestionCompteComponent } from './gestion-compte/gestion-compte.component';
import { HomeComponent } from './home/home.component';
import { MenusComponent } from './menus/menus.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { AuthGuardsGuard } from './services/guards/auth-guards.guard';
import { SubscribeComponent } from './subscribe/subscribe.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'subscribe', component: SubscribeComponent },
  { path: 'signin', component: ConnectionComponent },
  { path: 'menus', component: MenusComponent },
  { path: 'restaurant', component: RestaurantComponent },
  {
    path: '', canActivate: [AuthGuardsGuard], children: [
    { path: 'board', component: DashboardComponent },
    {path: 'myaccount', component: GestionCompteComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
