
export const Host="localhost";
export const Port="8000";

export const TimeoutToken=3600000;
export const TimeoutTokenInMinutes=60;

export const LoginURL="https://"+Host+":"+Port+"/api/login_check";
export const CreateUserURL="https://"+Host+":"+Port+"/api/users";
export const Menu = "https://"+Host+":"+Port+"/api/menus"
export const Restaurant = "https://"+Host+":"+Port+"/api/restaurants"
export const Commande = "https://"+Host+":"+Port+"/api/commandes"
export const User = "https://"+Host+":"+Port+"/api/users"
export const ListCommandes = "https://"+Host+":"+Port+"/api/listcommandes"
export const Reservation = "https://"+Host+":"+Port+"/api/reservations"
