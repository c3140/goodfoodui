import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Menus } from '../model/menu/menus';

import * as Environnement from 'src/app/Environnement'

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {

  constructor(private readonly http : HttpClient) { }

  Menu :Menus[];



  ngOnInit(): void {
    this.getMenus()
  }

  getMenus()
  {
    this.http.get<Menus[]>(Environnement.Menu).subscribe(
      res=>{
        this.Menu = Array.from(Object.values(res['hydra:member']));
      }
    );
  }

}
