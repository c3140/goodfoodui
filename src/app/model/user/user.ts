export class User {
      id : number
      email : string
      roles : string[]
      lastname_user : string
      firstname_user : string
      address_user : string
      city_user : string
      num_phone : string
      number_visit_user : number
      bonus_reduc_user : number
      birthdate_user : string
      listcommandes : string[]
      username : string
      addressUser : string
      password : string
      lastnameUser
      firstnameUser
      cityUser
      numPhone
      numberVisitUser
      bonusReducUser
      birthdateUser
      createAt
      createAtAgo
      updateAt
      last_visit_user
      lastVisitUser
      last_commande_user
      lastCommandeUser
}
